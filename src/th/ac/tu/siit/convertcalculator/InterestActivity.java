package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InterestActivity extends Activity implements OnClickListener {
	//int A,p,r=10,t;
	//A = p(1+r)^2;
	float vR;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);
		

		Button b1 = (Button)findViewById(R.id.btnConvertMoney);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.btnSetting);
		b2.setOnClickListener(this);


		TextView tvRate = (TextView)findViewById(R.id.tvRate);
		vR = Float.parseFloat(tvRate.getText().toString());
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}
	public void onClick(View v) {
		int id = v.getId();
		
		
		if (id == R.id.btnConvertMoney) {
			
		EditText edtTextDeposit = (EditText) findViewById(R.id.etdeposit);
		
		String sP = edtTextDeposit.getText().toString();
		
		
		
         EditText edtTextAmountofyears = (EditText) findViewById(R.id.Amountofyears);
		
         String sT = edtTextAmountofyears.getText().toString();
         
        
         double vP = Double.parseDouble(sP);
         double vT = Double.parseDouble(sT);
         
         double A = vP * Math.pow((1+(vR/100)), vT);
         
         
         
         String res = "";
			try {
				res = String.format(Locale.getDefault(), "%.2f", A);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			TextView tvTHB = (TextView)findViewById(R.id.tvTHB);
			
			tvTHB.setText(res);
			
		}
		
         
		
		//A = p (r+1)^t;
		/***
		if (id == R.id.btnLbKg) {
			Intent i = new Intent(this, WeightActivity.class);
			i.putExtra("defaultPound", 10.0f);
			startActivity(i);
		}
		else 
		***/
		if (id == R.id.btnUSDTHB) {
			Intent i = new Intent(this, ExchangeActivity.class);
			startActivity(i);
		}
		else if (id == R.id.btnSetting) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("exchangeRate", vR);
			startActivityForResult(i, 9999);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			vR = data.getFloatExtra("exchangeRate", 10.0f);
			TextView tvRate = (TextView)findViewById(R.id.tvRate);
			tvRate.setText(String.format(Locale.getDefault(), "%.2f", vR));
		}
	}
	}

